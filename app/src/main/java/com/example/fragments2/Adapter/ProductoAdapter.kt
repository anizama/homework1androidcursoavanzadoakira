package com.example.fragments2.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.fragments2.Producto
import com.example.fragments2.R


class ProductoAdapter(var productos: MutableList<Producto>):RecyclerView.Adapter<ProductoAdapter.ProductoAdapterViewHolder>() {

    class ProductoAdapterViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){

        fun bind(producto:Producto){

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductoAdapterViewHolder {
        val view:View = LayoutInflater.from(parent.context).inflate(R.layout.item_comida,parent,false) //cargar layout
        return ProductoAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return productos.size
    }

    override fun onBindViewHolder(holder: ProductoAdapterViewHolder, position: Int) {
        val producto = productos[position]
        holder.bind(producto)
    }


}