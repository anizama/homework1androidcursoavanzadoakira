package com.example.fragments2.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.fragments2.Adapter.ProductoAdapter
import com.example.fragments2.Producto
import com.example.fragments2.R
import kotlinx.android.synthetic.main.fragment_fragmentoestatico1.*


class fragmentoestatico1 : Fragment() {
    val productos = mutableListOf<Producto>()
    val adaptador by lazy {
        ProductoAdapter(productos)
    }
    //lateinit var adaptador: ProductoAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragmentoestatico1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cargarinformacion()
        configuraradaptador()
    }

    private fun configuraradaptador() {
        //adaptador= ProductoAdapter(productos)
        recyclercomidas.adapter = adaptador
        recyclercomidas.layoutManager = LinearLayoutManager(requireContext())

    }

    private fun cargarinformacion() {
        productos.add(Producto("Frutas","S/.20","","Las frutas son buenas para la salud "))
        productos.add(Producto("Verduras","S/.20","","las verduras ayudan a bajar de peso"))
        productos.add(Producto("Bebidas","S/.20","","las bebidas calman la sed"))
        productos.add(Producto("Golosinas","S/.20","","las golosinas no son buenas para bajar de peso"))
    }


}