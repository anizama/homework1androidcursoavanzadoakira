package com.example.fragments2

import java.io.Serializable

data class Producto (
        var nombre:String,
        var precio:String,
        var img:String,
        var mensaje:String
)
    : Serializable {
//ALT ENTER en key pokemon it.(Let pokemon interface serializable)

}
